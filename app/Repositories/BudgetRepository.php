<?php


namespace App\Repositories;

use App\Contracts\BudgetContract;
use App\Models\Budget;
use App\Models\User;
use App\Models\Transaction;
use Illuminate\Database\QueryException;

class BudgetRepository extends BaseRepository implements BudgetContract
{

    public function __construct(Budget $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function createBudget(array $parameters, User $user)
    {
        try {
            $budget = new Budget($parameters);
            $user->userOfBudget()->save($budget);
            return $budget;
        }
        catch (QueryException $e) {
            return $e;
        }
    }

    public function deleteBudget(int $id)
    {

        $this->delete($id);
    }

    public function listBudgetTransactions(int $budget)
    {
        $transactions = Transaction::where('budget_id', '=', $budget)->get();
        return response($transactions);
    }

}
