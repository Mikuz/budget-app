<?php


namespace App\Repositories;


use App\Contracts\TransactionsContract;
use App\Models\Budget;
use App\Models\Transaction;
use App\Models\Category;
use Illuminate\Database\QueryException;
use Illuminate\Support\Arr;

class TransactionsRepository extends BaseRepository implements TransactionsContract
{
    public function __construct(Transaction $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function listAllTransactions(){
        //todo
    }

    public function createTransaction(array $parameters){
        try {
            $budget = Budget::find(Arr::get($parameters, 'budget_id'));
            $category = Category::find(Arr::get($parameters, 'transaction_of_category_id'));

            $transaction = new Transaction($parameters);
            $transaction->transactionOfCategory()->associate($category);
            $budget->budgetOfTransaction()->save($transaction);
            return $transaction;
        }
        catch(QueryException $e){
            echo $e;
        }
    }

    public function deleteTransaction(int $id){
        $this->delete($id);
    }

}
