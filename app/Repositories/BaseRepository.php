<?php


namespace App\Repositories;

use App\Contracts\BaseContract;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements BaseContract
{
    protected $model;

    public function __construct(Model $model){
        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function find(int $id){
        return $this->model->find($id);
    }

    public function create(array $attributes){
        return $this->model->create($attributes);
    }

    public function update(){
       //
    }

    public function delete($id){
        try {
            return $this->model->find($id)->delete();
        } catch (\Exception $e) {
            abort(400);
        }
    }
}
