<?php


namespace App\Repositories;

use Illuminate\Database\QueryException;
use App\Models\Category;
use App\Contracts\CategoriesContract;


class CategoriesRepository extends BaseRepository implements CategoriesContract
{
    public function __construct(Category $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function listCategories()
    {
        return $this->getAll();
    }

    public function findCategoryById(int $id){
        return $this->find($id);
    }

    public function createCategory(array $parameters)
    {
        try {
            $category = new Category($parameters);
            $category->save();
            return $category;
        }
        catch (QueryException $e){
            return null;
        }
    }

    public function deleteCategory(int $id)
    {
            $this->delete($id);
    }

}
