<?php

namespace App\Http\Controllers;

use App\Models\Budget;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function getAllUsers(){
        $users = User::all();
        return $users;
    }

    public function getUserProfile(){
        $user = Auth::user();
        $userdata = array(
            "name"=>$user->name,
            "email"=>$user->email,
        );
        return view('profile', compact('userdata'));
    }

    public function getUserBudgets(){
        $user_id = Auth::user()->id;
        return $budgets = Budget::where('user_id', '=', $user_id)->get();
    }

    public function updateUserProfile(){

    return 'update';
    }
}
