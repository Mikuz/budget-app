<?php

namespace App\Http\Controllers;

use App\Models\Budget;
use App\Models\Category;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Eloquent;
use App\Contracts\TransactionsContract;

/**
 * Category
 *
 * @mixin Eloquent
 */

class TransactionController extends Controller
{
    protected $transactionRepository;

    public function __construct(TransactionsContract $transactionRepository){
        $this->transactionRepository = $transactionRepository;
    }

    public function createTransaction(Request $request){
        $data = $request->validate([
            'name' => 'required|max:255',
            'amount' => 'required|numeric',
            'description' => 'required|max:255',
            'type' => 'required',
            'budget_id' => 'required',
            'transaction_of_category_id' => 'required',
        ]);

        return $this->transactionRepository->createTransaction($data);
    }

    public function deleteTransaction($id){
        $this->transactionRepository->deleteTransaction($id);
    }
}
