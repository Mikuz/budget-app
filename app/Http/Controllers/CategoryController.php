<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\CategoriesContract;
use App\Models\Category;


class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoriesContract $categoryRepository){
        $this->categoryRepository = $categoryRepository;
    }

    public function showCategories(Request $request){

        return $categories = $this->categoryRepository->listCategories();
    }

    public function addCategory(Request $request){
        $data = $request->validate([
            'name' => 'required|max:100',
            'color' => 'required',
        ]);

        return $this->categoryRepository->createCategory($data);
    }

    public function deleteCategory($id){
        $this->categoryRepository->deleteCategory($id);
    }

    public function findCategory($id){
        $categories = $this->categoryRepository->findCategoryById($id);

        return view('categories', compact('categories'));
    }



}
