<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use App\Contracts\BudgetContract;
use App\Models\Budget;
use Illuminate\Support\Facades\Auth;

class BudgetController extends Controller
{
    protected $budgetRepository;

    public function __construct(BudgetContract $budgetRepository)
    {
        $this->budgetRepository = $budgetRepository;
    }

    public function createBudget(Request $request)
    {

        $current_user = Auth::user();

        $data = $request->validate([
            'name' => 'required|max:255',
            'amount' => 'required|numeric',
            'period' => 'required',
            'currency' => 'required',
        ]);

        return $this->budgetRepository->createBudget($data, $current_user);
    }

    public function deleteBudget($id)
    {
        $this->budgetRepository->deleteBudget($id);
    }

    public function listBudgetTransactions(Request $request, $budget)
    {
        return $transactions = $this->budgetRepository->listBudgetTransactions($budget);
    }
}

