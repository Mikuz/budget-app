<?php


namespace App\Contracts;


use App\Models\User;
use App\Models\Budget;

interface BudgetContract
{
    public function createBudget(array $parameters,User $user);

    public function deleteBudget(int $id);

    public function listBudgetTransactions(int $budget);

}
