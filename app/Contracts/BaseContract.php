<?php


namespace App\Contracts;


interface BaseContract
{
    public function getAll();

    public function find(int $id);

    public function create(array $attributes);

    public function update();

    public function delete(int $id);
}
