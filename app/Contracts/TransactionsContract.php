<?php


namespace App\Contracts;
use App\Models\Budget;
use App\Models\Category;
use App\Models\Transaction;


interface TransactionsContract
{

    public function listAllTransactions();

    public function createTransaction(array $parameters);

    public function deleteTransaction(int $id);

}
