<?php


namespace App\Contracts;


interface CategoriesContract
{

    public function listCategories();

    public function findCategoryById(int $id);

    public function createCategory(array $parameters);

    public function deleteCategory(int $id);
}
