<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Eloquent;

/**
 * Budget
 *
 * @mixin Eloquent
 */

class Budget extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'amount',
        'period',
        'currency',
    ];

    protected $table = 'budget';

    public function budgetOfTransaction()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    public function budgetOfUser()
    {
        return $this->belongsTo('App\Models\User');
    }
}
