<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Eloquent;

/**
 * Category
 *
 * @mixin Eloquent
 */

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'color'
    ];

    protected $table = 'category';

    public function categoryOfTransaction(){
        return $this->hasOne('\App\Models\Transaction', 'transaction_of_category_id');
    }
}
