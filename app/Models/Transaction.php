<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Eloquent;

/**
 * Transaction
 *
 * @mixin Eloquent
 */

class Transaction extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'amount',
        'type',
    ];

    protected $table = 'transaction';

    public function transactionOfBudget()
    {
        return $this->belongsTo('App\Models\Budget');
    }

    public function transactionOfCategory(){
        return $this->belongsTo('App\Models\Category');
    }
}
