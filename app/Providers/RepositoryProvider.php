<?php

namespace App\Providers;

use App\Contracts\TransactionsContract;
use App\Repositories\TransactionsRepository;
use Illuminate\Support\ServiceProvider;
use App\Contracts\CategoriesContract;
use App\Repositories\CategoriesRepository;
use App\Contracts\BudgetContract;
use App\Repositories\BudgetRepository;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CategoriesContract::class, CategoriesRepository::class);
        $this->app->bind(BudgetContract::class, BudgetRepository::class);
        $this->app->bind(TransactionsContract::class, TransactionsRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
