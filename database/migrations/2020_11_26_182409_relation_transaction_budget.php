<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RelationTransactionBudget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('transaction', function (Blueprint $table) {
            $table->unsignedBigInteger('budget_id');
            $table->foreign('budget_id')->references('id')->on('budget')->onDelete('cascade');
        });

        Schema::table('transaction', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_of_category_id');
            $table->foreign('transaction_of_category_id')->references('id')->on('category');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
