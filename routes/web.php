<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/users', [UserController::class, 'getAllUsers']) -> name('user.getallusers')->middleware('auth');
Route::get('/profile', [UserController::class, 'getUserProfile']) -> name('user.getuserprofile')->middleware('auth');
Route::get('/budgets/get', [\App\Http\Controllers\UserController::class, 'getUserBudgets'])->name('userBudgets');
Route::post('/profile/update', [UserController::class, 'updateUserProfile']) -> name('user.updateuserprofile')->middleware('auth');

Route::get('/categories/get', [\App\Http\Controllers\CategoryController::class,'showCategories'])->name('showCategories');
Route::get('/categories/{id}', [\App\Http\Controllers\CategoryController::class,'findCategory']);
Route::delete('/categories/remove/{id}', [\App\Http\Controllers\CategoryController::class,'deleteCategory']);
Route::post('/categories/new', [\App\Http\Controllers\CategoryController::class, 'addCategory'])->name('createCategory');

Route::get('/budget/{id}/transactions', [\App\Http\Controllers\BudgetController::class, 'listBudgetTransactions'])->name('budgetTransactions');
Route::delete('/budget/remove/{id}', [\App\Http\Controllers\BudgetController::class, 'deleteBudget'])->name('deleteBudget');
Route::post('/budget/create', [\App\Http\Controllers\BudgetController::class, 'createBudget'])->name('createBudget');

Route::delete('/transaction/remove/{id}', [\App\Http\Controllers\TransactionController::class, 'deleteTransaction'])->name('deleteTransaction');
Route::post('/budget/transaction/new',[\App\Http\Controllers\TransactionController::class, 'createTransaction'])->name('createTransactionInBudget');

Route::get('/{any}', function(){
    return view('app');
})->where('any', '.*');
