@extends('app')

@section('content')

    <h1>budget transactions here!</h1>
    {{$transactions}}

    <div class="card-body">
        <form method="POST" action="{{ route('createTransactionInBudget') }}">
            @csrf
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text"  class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                </div>
            </div>
            {{--        dodac wiadomosc o bledzie--}}

            <div class="form-group row">
                <label for="amount" class="col-md-4 col-form-label text-md-right">{{ __('amount') }}</label>

                <div class="col-md-6">
                    <input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}" required autocomplete="amount">

                </div>
            </div>

            <div class="form-group row">
                <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('description') }}</label>

                <div class="col-md-6">
                    <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" required autocomplete="description">

                </div>
            </div>

            <div class="form-group row">
                <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('type') }}</label>

                <div class="col-md-6">
                    <input id="type" type="text" class="form-control" name="type" value="{{ old('type') }}" required autocomplete="type">

                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Add transaction to budget') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
