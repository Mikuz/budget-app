@extends('app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
                <b-box>
                    @guest
                        @if (Route::has('login'))

                                <button class="btn-lg btn-primary"><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></button>
                            </li>
                        @endif

                        @if (Route::has('register'))
                                <button class="btn-lg btn-primary"><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></button>
                        @endif
                    @endguest
                            </b-box>
{{--                    @if (session('status'))--}}
{{--                        <div class="alert alert-success" role="alert">--}}
{{--                            {{ session('status') }}--}}
{{--                        </div>--}}
{{--                    @endif--}}
    </div>
</div>
@endsection
