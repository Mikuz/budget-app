/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Buefy from 'buefy'
import VueRouter from 'vue-router'
import 'buefy/dist/buefy.css'
import 'vue-moment'
import Axios from 'axios'

Vue.use(Buefy);
Vue.use(require('vue-moment'));
Vue.use(VueRouter)

import BudgetComponent from "./components/BudgetComponent";
import CategoryComponent from "./components/CategoryComponent";
import App from "./components/App";
import BudgetTransactionsComponent from "./components/BudgetTransactionsComponent";

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/budgets',
            name: 'budgets',
            component: BudgetComponent,
            children:[
                {
                    path: ':/transactions',
                    component: BudgetTransactionsComponent,
                    props:{
                        budget: Number,
                        categories: Array,
                    }
                }
            ]
        },
        {
            path: '/categories',
            name: 'categories',
            component: CategoryComponent,
        },
        {
            path:'/transactions',
            name:'transactions',
            component: BudgetTransactionsComponent,
            props:{
                budget: '',
            }
        }
    ],
})

const app = new Vue({
    el: '#app',
    router,
    components: {
        'app': App,
    },
});
